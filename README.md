# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How to setup Arduino IDE ###
* install arduino IDE, not from apt-get install, donwload zip, and run ./install.sh (the version in repository is "marada")
* File-> Preferences-> Additional Boards: Add: http://arduino.esp8266.com/stable/package_esp8266com_index.json
* Tools -> Boards -> BoardManager... Find esp8266, install (at 19/12/2020 version 2.7.1)
* Tools -> Boards : find and select Generic esp8266 Board
* Flash size, cpu frequency etc... all configuration probably are ok, is some problems, google it!
* Tools -> Port (select the correct that only apears after connect usd converter)
* To upload code, must be in flashmode. click button, and apply energy.

### Using This Project ###

* Put the code into sonoff
* Turn on, and the led will blink very fast during 3 seconds. Click push button in this time windows
* Now, you are in the AP mode to be abble to configure the wifi connection.
* Connect to the AP that sonoff put to work, SSID XXXXXXXX, password xxxxxxxx.
* Connect to http://192.168.4.1
* Fill the SSID and Password with your LAN credentials
* The sonnoff will restart and blick very fast again. Do not click in button
* After a 3 seconds fast blick, will blink more slow, is try to connect to your local network
* If cant connect after some time, continue and work in "stand alone" mode.

### EndPoins Available ###
* /info
* /status
* /on
* /off
* /toggle
* /dashboard

### Another Features ###
* If press button on sonoff and stay pushed, the light starts on and off 5 times, and restart.

### ChangeLog ###
* Added websockets
* Added /dashboard