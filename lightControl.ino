/**
 * @author David Pintassilgo & Marco Martins
 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WebSocketsServer.h>

 
const char *APssid = "XXXXXXXX";
const char *APpassword = "xxxxxxxx";

String deviceInfo = "{\"DNM\": \"Luz da Bancada\", \"DCD\": \"111\", \"DMD\": \"SONOFF\"}";
String statusON = "{\"RLa\": 1}";
String statusOFF = "{\"RLa\": 0}";
String dashboardHtml = "";

boolean apMode = false;

MDNSResponder mdns;
ESP8266WebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);


int gpioButton0 = 0;
int gpioLed13 = 13;
int gpioRelay12 = 12;

int resetCounter = 0;

void setup() {

  pinMode(gpioButton0, INPUT);  
  pinMode(gpioLed13, OUTPUT);
  pinMode(gpioRelay12, OUTPUT);
  
  Serial.begin(115200);
  SPIFFS.begin();
  webSocket.begin();
  
  int i=0;
  while (i<40) {
    digitalWrite(gpioLed13, LOW);
    delay(50);
    digitalWrite(gpioLed13, HIGH);
    delay(50);
    i++;
    if (digitalRead(gpioButton0)==LOW){
      WiFi.persistent(false);
      WiFi.softAP(APssid, APpassword);
      digitalWrite(gpioLed13, LOW);
      
      server.on("/", handleRoot);
      server.on("/config", handleConfig);
      server.begin();
      apMode = true;
      break;      
    }
  }

  if (!apMode) {
    String credentials = "x x";
    File f = SPIFFS.open("/conf.txt", "r");
    if (!f) {
        Serial.println("Error reading file.");
    } else {
      while(f.available()) {
        credentials = f.readStringUntil('\n');
        Serial.print("File readed:   ");
        Serial.println(credentials);
        break;
      } 
    }
    f.close();

    String s = "";
    String p = "";
    for (int i=0 ; i < credentials.length(); i++) {
      if (credentials.charAt(i) == ' ') {
          s = credentials.substring(0,i);
          p = credentials.substring(i+1,credentials.length()-1);   
      }
    }
    
    WiFi.begin(s, p);
    int timeout = 50;
    while (WiFi.status() != WL_CONNECTED) {
      digitalWrite(gpioLed13, LOW);
      delay(150);
      digitalWrite(gpioLed13, HIGH);
      delay(200);
      timeout --;
      if (timeout <=0) {
        break;
      }
    }
    
    if (mdns.begin("esp8266", WiFi.localIP())) {
      Serial.println("MDNS responder started");

      server.on("/info", [](){
      server.sendHeader("Access-Control-Allow-Origin","*");
      server.send(200, "application/json", deviceInfo);
      });

      server.on("/status", [](){
        server.sendHeader("Access-Control-Allow-Origin","*");
        if(digitalRead(gpioRelay12)==HIGH) {
          server.send(200, "application/json", statusON);
        } else {
          server.send(200, "application/json", statusOFF);
        }
      });
  
    server.on("/on", [](){
      turnON();
      server.sendHeader("Access-Control-Allow-Origin","*");
      server.send(200, "application/json", statusON);
    });
  
    server.on("/off", [](){
      turnOFF();
      server.sendHeader("Access-Control-Allow-Origin","*");
      server.send(200, "application/json", statusOFF);
    });

    server.on("/toggle", [](){
      toggle();
      server.sendHeader("Access-Control-Allow-Origin","*");
      server.send(200, 
                  "application/json", 
                  (digitalRead(gpioRelay12)==LOW) ? statusOFF : statusON );
    });

    server.on("/dashboard", [](){
      String myIP = WiFi.localIP().toString();
      server.sendHeader("Access-Control-Allow-Origin","*");
      server.send(200, 
                  "text/html", 
                  buildDashBoard(myIP) );
    });

    
      server.begin();
    }
      
    turnON();
    
  }
} 
void loop() {
  webSocket.loop();
  server.handleClient();
  if (digitalRead(gpioButton0)==LOW){
    if (!apMode) {
      toggle(); 
    }
    resetCounter++;
  } else {
    resetCounter = 0;  
  }

  if (resetCounter >=10) {
    delay(1000);
    ESP.restart();  
  }
}

void turnOFF(void) {
  webSocket.broadcastTXT(statusOFF);
  digitalWrite(gpioLed13, HIGH);
  digitalWrite(gpioRelay12, LOW);
  delay(500);
}

void turnON(void) {
  webSocket.broadcastTXT(statusON);
  digitalWrite(gpioLed13, LOW);
  digitalWrite(gpioRelay12, HIGH);
  delay(500);
}

void toggle(void) {
  (digitalRead(gpioRelay12)==LOW) ? turnON() : turnOFF();
}

void handleRoot() {
  server.send(200, "text/html", 
  "<html><body><h1>Configurations:</h1><form action='/config' method='get'><label for='ssid'>SSID:</label><br><input type='text' id='ssid' name='ssid'><br><label for='pwd'>PASSWORD:</label><br><input type='text' id='pwd' name='pwd'><br><input type='submit' value='Submit'></form></body></html>");
}

void handleConfig() {
 
  String credentials = server.arg("ssid") + " " + server.arg("pwd");

  File f = SPIFFS.open("/conf.txt", "w");

  if (f) {
    f.println(credentials);
    f.close();
    server.send(200, "text/html", credentials + " Added to the file. Reboting in 5 seconds ...");
    delay(5000);
  } else {
    server.send(200, "text/html", "Error. Reboting!");
  }
  ESP.restart();
}

String buildDashBoard(String ip) {
  String html = "<!DOCTYPE html><html><head><meta charset='utf-8'/></head><body><p>Luz da Bancada</p><button id='button' onclick='window.click()' ></button></body><script>let socket;document.addEventListener('DOMContentLoaded', function(event) {let url = 'http://<IP>/status';var request = new XMLHttpRequest();request.open('GET', url);request.responseType = 'json';request.send();request.onload = function() {console.log(request.response['RLa']);if (request.response['RLa'] == 0){document.getElementById('button').innerHTML='Turn ON';} else {document.getElementById('button').innerHTML='Turn OFF';}};socket = new WebSocket('ws://<IP>:81');socket.onmessage = function(event) {let jsonobj = JSON.parse(event.data);if(jsonobj['RLa']=='1'){document.getElementById('button').innerHTML='Turn OFF';} else {document.getElementById('button').innerHTML='Turn ON';}}});function click() {const Http = new XMLHttpRequest();const url='http://<IP>/toggle';Http.open('GET', url);Http.send();}</script></html>";
  html.replace("<IP>", ip);
  return html;
}